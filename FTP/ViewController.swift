//
//  ViewController.swift
//  FTP
//
//  Created by NetPyx on 19/06/17.
//  Copyright © 2017 NetPyx. All rights reserved.
//

import UIKit

var globalUrl : String = String("http://netpyx.in/os2fman/api/")
class ViewController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgUsername: UIImageView!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblSignIn: UILabel!
    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    
    @IBOutlet weak var imgArrow: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            imgUsername.layer.borderWidth = 1
            imgPassword.layer.borderWidth = 1
            imgUsername.layer.cornerRadius = 10.0;
            imgPassword.layer.cornerRadius = 10.0;
            btnLogin.layer.cornerRadius = 10.0;
        }
        else
        {
            imgUsername.layer.borderWidth = 1
            imgPassword.layer.borderWidth = 1
            imgUsername.layer.cornerRadius = 5.0;
            imgPassword.layer.cornerRadius = 5.0;
            btnLogin.layer.cornerRadius = 5.0;
        }
        
        self.activityInd.hidden = true
        imgUsername.layer.borderColor = UIColor(red: 135, green: 142, blue: 150, alpha: 1.0).CGColor
        imgPassword.layer.borderColor = UIColor(red: 135, green: 142, blue: 150, alpha: 1.0).CGColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Actions
    @IBAction func btnActionLogin(sender: AnyObject)
    {
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        if ReachabilityManager.sharedInstance.isConnectedToNetwork()
        {
            if (txtUsername.text == "")
            {
                let alert = UIAlertController(title: "Alert!", message: "Please enter user token.", preferredStyle:.Alert)
                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                    print(action)
                }
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else if(txtPassword.text == "")
            {
                let alert = UIAlertController(title: "Alert!", message: "Please enter auth key.", preferredStyle:.Alert)
                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                    print(action)
                }
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                self.activityInd.startAnimating()
                self.imgArrow.hidden = true
                self.activityInd.hidden = false
                self.lblSignIn.text = "Signing In"
                self.btnLogin.alpha = 0.5
                let session = NSURLSession.sharedSession()
                let request = NSMutableURLRequest(URL: NSURL(string: globalUrl)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
                
                // create some JSON data and configure the request
                let jsonString = String(format: "uname=%@&pwd=%@&api_type=login_via_api", txtUsername.text!, txtPassword.text!)
                request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
                request.HTTPMethod = "POST"
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                
                let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                    if let error = error {
                        print(error)
                        self.activityInd.stopAnimating()
                        self.activityInd.hidden = true
                        self.imgArrow.hidden = false
                        self.lblSignIn.text = "Sign In"
                        self.btnLogin.alpha = 1.0
                        let alert = UIAlertController(title: "Alert!", message: error.localizedDescription, preferredStyle:.Alert)
                        let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                            print(action)
                        }
                        alert.addAction(cancelAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    if let data = data{
                        print("data =\(data)")
                    }
                    if let response = response {
                        // print("url = \(response.URL!)")
                        print("response = \(response)")
                        // let httpResponse = response as! NSHTTPURLResponse
                        // print("response code = \(httpResponse.statusCode)")
                        
                        //if you response is json do the following
                        do{
                            let resultJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())
                            let arrayJSON = resultJSON as! NSDictionary
                            
                            if(arrayJSON.objectForKey("type") as! String == "success")
                            {
                                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "Is_Login")
                                NSUserDefaults.standardUserDefaults().setValue(arrayJSON.objectForKey("party_id"), forKey: "Party_Id")
                                NSUserDefaults.standardUserDefaults().setValue(arrayJSON.objectForKey("name_of_person"), forKey: "User_Name")
                                NSUserDefaults.standardUserDefaults().setValue(self.txtPassword.text, forKey: "Password")
                                dispatch_async(dispatch_get_main_queue(),{
                                    self.activityInd.stopAnimating()
                                    self.activityInd.hidden = true
                                    self.imgArrow.hidden = false
                                    self.lblSignIn.text = "Sign In"
                                    self.btnLogin.alpha = 1.0
                                    self.txtUsername.text = ""
                                    self.txtPassword.text = ""
                                    self.performSegueWithIdentifier("PushToHomeFromLogin", sender: self)
                                })
                            }
                            else
                            {
                                dispatch_async(dispatch_get_main_queue(),{
                                    self.activityInd.stopAnimating()
                                    self.activityInd.hidden = true
                                    self.imgArrow.hidden = false
                                    self.lblSignIn.text = "Sign In"
                                    self.btnLogin.alpha = 1.0
                                    let alert = UIAlertController(title: "Alert!", message: arrayJSON.objectForKey("message") as? String, preferredStyle:.Alert)
                                    let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                        print(action)
                                    }
                                    alert.addAction(cancelAction)
                                    self.presentViewController(alert, animated: true, completion: nil)
                                })
                            }
                            
                        }catch _{
                            print("Received not-well-formatted JSON")
                        }
                    }
                })
                task.resume()
            }
        }
        else
        {
            let alert = UIAlertController(title: "Alert!", message: "No Internet Connection.", preferredStyle:.Alert)
            let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                print(action)
            }
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - - UITextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField!) {
        if (textField == txtUsername)
        {
            self.view.frame = CGRectMake(self.view.frame.origin.x, -50, self.view.frame.size.width, self.view.frame.size.height);
        }
        else if (textField == txtPassword)
        {
            self.view.frame = CGRectMake(self.view.frame.origin.x, -100, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        if (textField == txtUsername)
        {
            txtPassword.becomeFirstResponder()
        }
        else if (textField == txtPassword)
        {
            txtPassword.resignFirstResponder()
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    //MARK: - MBProgressHud Methods
    func startActivityIndicator() {
        
        let spinnerActivity = MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        
        spinnerActivity.label.text = "Logging in..";
        
        spinnerActivity.userInteractionEnabled = false;
        
    }
    
    func stopActivityIndicator() {
        MBProgressHUD.hideHUDForView(self.view, animated: true);
        
    }

}

