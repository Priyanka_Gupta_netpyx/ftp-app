//
//  HomeViewController.swift
//  FTP
//
//  Created by NetPyx on 19/06/17.
//  Copyright © 2017 NetPyx. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var lblHost: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lbl1: UITextView!
    @IBOutlet weak var lbl2: UITextView!
    @IBOutlet weak var lbl3: UITextView!
    @IBOutlet weak var lblConnect: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var detailActInd: UIActivityIndicatorView!
    
    @IBOutlet weak var btnMail: UIButton!
    @IBOutlet weak var mailActInd: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            btnStatus.layer.cornerRadius = 10.0;
            btnDetail.layer.cornerRadius = 10.0;
            statusView.layer.cornerRadius = 10.0;
            statusView.layer.borderWidth = 2
        }
        else
        {
            btnStatus.layer.cornerRadius = 5.0;
            btnDetail.layer.cornerRadius = 5.0;
            statusView.layer.cornerRadius = 5.0;
            statusView.layer.borderWidth = 1
        }
        statusView.layer.borderColor = UIColor.darkGrayColor().CGColor
        lbl1.contentInset = UIEdgeInsetsMake(-7.0,0.0,0,0.0);
        lbl2.contentInset = UIEdgeInsetsMake(-7.0,0.0,0,0.0);
        lbl3.contentInset = UIEdgeInsetsMake(-7.0,0.0,0,0.0);
        self.activityIndicator.hidden = true
        self.detailActInd.hidden = true
        self.mailActInd.hidden = true
        
        imgStatus.layer.cornerRadius = imgStatus.frame.size.width/2
        
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: - Button Actions
    
    @IBAction func btnActionDetail(sender: AnyObject)
    {
        if ReachabilityManager.sharedInstance.isConnectedToNetwork()
        {
            self.statusView.hidden = true
            self.statusView.frame = CGRectMake(self.statusView.frame.origin.x, self.btnDetail.frame.origin.y+2, self.statusView.frame.size.width, self.statusView.frame.size.height)
            self.detailActInd.startAnimating()
            self.detailActInd.hidden = false
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest(URL: NSURL(string: globalUrl)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
            
            // create some JSON data and configure the request
            let jsonString = String(format: "party_id=%@&pwd=%@&api_type=get_ftp_user_details", NSUserDefaults.standardUserDefaults().valueForKey("Party_Id") as! String, NSUserDefaults.standardUserDefaults().valueForKey("Password") as! String)
            request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            request.HTTPMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                if let error = error {
                    print(error)
                    self.detailActInd.stopAnimating()
                    self.detailActInd.hidden = true
                    let alert = UIAlertController(title: "Alert!", message: error.localizedDescription, preferredStyle:.Alert)
                    let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                        print(action)
                    }
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                if let data = data{
                    print("data =\(data)")
                }
                if let response = response {
                    // print("url = \(response.URL!)")
                    print("response = \(response)")
                    // let httpResponse = response as! NSHTTPURLResponse
                    // print("response code = \(httpResponse.statusCode)")
                    
                    //if you response is json do the following
                    do{
                        let resultJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())
                        let arrayJSON = resultJSON as! NSDictionary
                        
                        if(arrayJSON.objectForKey("type") as! String == "success")
                        {
                            //self.btnStatus.hidden = true
                            dispatch_async(dispatch_get_main_queue(),{
                                self.detailActInd.stopAnimating()
                                self.detailActInd.hidden = true
                                self.lblHost.hidden = false
                                self.lblUsername.hidden = false
                                self.lblPassword.hidden = false
                                self.lbl1.hidden = false
                                self.lbl2.hidden = false
                                self.lbl3.hidden = false
                                self.lblConnect.hidden = true
                                
                                self.lbl1.text = arrayJSON.objectForKey("data")?.objectForKey("ftp_server") as? String
                                self.lbl2.text = arrayJSON.objectForKey("data")?.objectForKey("ftp_username") as? String
                                self.lbl3.text = arrayJSON.objectForKey("data")?.objectForKey("ftp_password") as? String
                                
                                UIView.animateWithDuration(1.0,
                                    delay: 0.0,
                                    options: .CurveEaseInOut,
                                    animations: {
                                        self.statusView.hidden = false
                                        self.statusView.frame = CGRectMake(self.statusView.frame.origin.x, self.btnDetail.frame.origin.y+self.btnDetail.frame.size.height+5, self.statusView.frame.size.width, self.statusView.frame.size.height)
                                        
                                    },
                                    completion: { finished in
                                        
                                })
                            })
                        }
                        else
                        {
                            dispatch_async(dispatch_get_main_queue(),{
                                self.detailActInd.stopAnimating()
                                self.detailActInd.hidden = true
                                let alert = UIAlertController(title: "Alert!", message: arrayJSON.objectForKey("message") as? String, preferredStyle:.Alert)
                                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                    print(action)
                                }
                                alert.addAction(cancelAction)
                                self.presentViewController(alert, animated: true, completion: nil)
                            })
                        }
                        
                    }catch _{
                        print("Received not-well-formatted JSON")
                    }
                }
            })
            task.resume()
        }
        else
        {
            let alert = UIAlertController(title: "Alert!", message: "No Internet Connection.", preferredStyle:.Alert)
            let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                print(action)
            }
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnActionStatus(sender: AnyObject)
    {
        if ReachabilityManager.sharedInstance.isConnectedToNetwork()
        {
            // self.statusView.hidden = true
            imgStatus.hidden = true
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest(URL: NSURL(string: globalUrl)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
            
            // create some JSON data and configure the request
            let jsonString = String(format: "party_id=%@&pwd=%@&api_type=get_ftp_status", NSUserDefaults.standardUserDefaults().valueForKey("Party_Id") as! String, NSUserDefaults.standardUserDefaults().valueForKey("Password") as! String)
            request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            request.HTTPMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                if let error = error {
                    print(error)
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                    let alert = UIAlertController(title: "Alert!", message: error.localizedDescription, preferredStyle:.Alert)
                    let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                        print(action)
                    }
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                if let data = data{
                    print("data =\(data)")
                }
                if let response = response {
                    // print("url = \(response.URL!)")
                    print("response = \(response)")
                    // let httpResponse = response as! NSHTTPURLResponse
                    // print("response code = \(httpResponse.statusCode)")
                    
                    //if you response is json do the following
                    do{
                        let resultJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())
                        let arrayJSON = resultJSON as! NSDictionary
                        
                        if(arrayJSON.objectForKey("type") as! String == "success")
                        {
                            
                            dispatch_async(dispatch_get_main_queue(),{
                                // self.btnDetail.hidden = true
                                self.activityIndicator.stopAnimating()
                                self.activityIndicator.hidden = true
                                if(arrayJSON.objectForKey("ftp_status") as! String == "green")
                                {
                                    self.imgStatus.image = UIImage(named: "icon_connected")
                                    self.animate(self.imgStatus)
                                }
                                else
                                {
                                    self.imgStatus.image = UIImage(named: "icon_disconnets")
                                    self.animate(self.imgStatus)
                                }
                            })
                        }
                        else
                        {
                            dispatch_async(dispatch_get_main_queue(),{
                                self.activityIndicator.stopAnimating()
                                self.activityIndicator.hidden = true
                                let alert = UIAlertController(title: "Alert!", message: arrayJSON.objectForKey("message") as? String, preferredStyle:.Alert)
                                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                    print(action)
                                }
                                alert.addAction(cancelAction)
                                self.presentViewController(alert, animated: true, completion: nil)
                            })
                        }
                        
                    }catch _{
                        print("Received not-well-formatted JSON")
                    }
                }
            })
            task.resume()
        }
        else
        {
            let alert = UIAlertController(title: "Alert!", message: "No Internet Connection.", preferredStyle:.Alert)
            let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                print(action)
            }
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnActionLogout(sender: AnyObject)
    {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("Is_Login")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("Party_Id")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("User_Name")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("Password")
        
        for (controller) in self.navigationController!.viewControllers
        {
            if (controller.isKindOfClass(ViewController))
            {
                self.navigationController?.popToViewController(controller, animated: false)
                break;
            }
        }
        self.navigationController?.pushViewController(self.storyboard!.instantiateViewControllerWithIdentifier("LoginView") as UIViewController, animated: false)
    }
    
    @IBAction func btnActionCancel(sender: AnyObject)
    {
        //self.btnDetail.hidden = false
        self.statusView.hidden = true
        //self.btnStatus.hidden = false
        UIView.animateWithDuration(1.0,
                                   delay: 0.0,
                                   options: .CurveEaseInOut,
                                   animations: {
                                    self.statusView.hidden = true
                                    self.statusView.frame = CGRectMake(self.statusView.frame.origin.x, self.btnDetail.frame.origin.y+2, self.statusView.frame.size.width, self.statusView.frame.size.height)
                                    
            },
                                   completion: { finished in
                                    
        })
    }
    
    
    @IBAction func btnActionMail(sender: AnyObject)
    {
        if ReachabilityManager.sharedInstance.isConnectedToNetwork()
        {
            self.btnMail.hidden = true
            self.mailActInd.startAnimating()
            self.mailActInd.hidden = false
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest(URL: NSURL(string: globalUrl)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
            
            // create some JSON data and configure the request
            let jsonString = String(format: "party_id=%@&pwd=%@&api_type=send_ftp_details_mail", NSUserDefaults.standardUserDefaults().valueForKey("Party_Id") as! String, NSUserDefaults.standardUserDefaults().valueForKey("Password") as! String)
            request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            request.HTTPMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                if let error = error {
                    print(error)
                    self.btnMail.hidden = false
                    self.mailActInd.stopAnimating()
                    self.mailActInd.hidden = true
                    let alert = UIAlertController(title: "Alert!", message: error.localizedDescription, preferredStyle:.Alert)
                    let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                        print(action)
                    }
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                if let data = data{
                    print("data =\(data)")
                }
                if let response = response {
                    // print("url = \(response.URL!)")
                    print("response = \(response)")
                    // let httpResponse = response as! NSHTTPURLResponse
                    // print("response code = \(httpResponse.statusCode)")
                    
                    //if you response is json do the following
                    do{
                        let resultJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())
                        let arrayJSON = resultJSON as! NSDictionary
                        
                        if(arrayJSON.objectForKey("type") as! String == "success")
                        {
                            //self.btnStatus.hidden = true
                            dispatch_async(dispatch_get_main_queue(),{
                                self.btnMail.hidden = false
                                self.mailActInd.stopAnimating()
                                self.mailActInd.hidden = true
                                
                                let alert = UIAlertController(title: "Alert!", message: arrayJSON.objectForKey("message") as? String, preferredStyle:.Alert)
                                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                    print(action)
                                }
                                alert.addAction(cancelAction)
                                self.presentViewController(alert, animated: true, completion: nil)
                                
                            })
                        }
                        else
                        {
                            dispatch_async(dispatch_get_main_queue(),{
                                self.btnMail.hidden = false
                                self.mailActInd.stopAnimating()
                                self.mailActInd.hidden = true
                                let alert = UIAlertController(title: "Alert!", message: arrayJSON.objectForKey("message") as? String, preferredStyle:.Alert)
                                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                    print(action)
                                }
                                alert.addAction(cancelAction)
                                self.presentViewController(alert, animated: true, completion: nil)
                            })
                        }
                        
                    }catch _{
                        print("Received not-well-formatted JSON")
                    }
                }
            })
            task.resume()
        }
        else
        {
            let alert = UIAlertController(title: "Alert!", message: "No Internet Connection.", preferredStyle:.Alert)
            let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                print(action)
            }
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    func animate(view : UIView)
    {
        view.hidden = false
        view.transform = CGAffineTransformMakeScale(0.00001, 0.00001);
            UIView.animateWithDuration(0.1,
                                       animations:{
                                        view.transform = CGAffineTransformMakeScale(0.95, 0.95);
                },
                                       completion:{ finished in
                                        
                                        UIView.animateWithDuration(0.1,
                                            animations:{
                                                view.transform = CGAffineTransformMakeScale(1.02, 1.02);
                                            },
                                            completion:{ finished in
                                                UIView.animateWithDuration (0.1,
                                                    animations:{
                                                        view.transform = CGAffineTransformIdentity;
                                                    },
                                                    completion:{ finished in
                                                        
                                                })
                                        })
            })
        
        UIView.animateWithDuration(0.3,
                                   delay:0.0,
                                   options:.CurveEaseOut,
                                   animations:{
                                    view.alpha = 1.0;
                                    view.transform = CGAffineTransformMakeScale(1.05 , 1.0)
            },
                                   completion: { finished in
        });
    }
    
    //MARK: - MBProgressHud Methods
    func startActivityIndicator() {
        
        let spinnerActivity = MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        
        spinnerActivity.label.text = "Please Wait..";
        
        spinnerActivity.userInteractionEnabled = false;
        
    }
    
    func stopActivityIndicator() {
        MBProgressHUD.hideHUDForView(self.view, animated: true);
        
    }
}
